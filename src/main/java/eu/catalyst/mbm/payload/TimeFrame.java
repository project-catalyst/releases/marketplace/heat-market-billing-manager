/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.catalyst.mbm.payload;

import java.io.Serializable;

public class TimeFrame implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer id;

	private String type;

	public TimeFrame() {
	}

	public TimeFrame(Integer id) {
		this.id = id;
	}

	public TimeFrame(Integer id, String type) {
		this.id = id;
		this.type = type;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	public boolean equals(Object object) {

		if (!(object instanceof TimeFrame)) {
			return false;
		}
		TimeFrame other = (TimeFrame) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	public String toString() {
		return "eu.catalyst.marketplace.model.TimeFrame[ id=" + id + " ]";
	}

}
