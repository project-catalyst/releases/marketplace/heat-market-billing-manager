package eu.catalyst.mbm.payload;

import java.io.Serializable;
import java.math.BigDecimal;

public class MarketActionCounterOffer implements Serializable {
	private static final long serialVersionUID = 1L;


	private Integer id;
	private Integer marketAction_Bid_id;
	private Integer marketAction_Offer_id;
	private BigDecimal exchangedValue;

	public MarketActionCounterOffer() {
	}

	public MarketActionCounterOffer(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getMarketAction_Bid_id() {
		return marketAction_Bid_id;
	}

	public void setMarketAction_Bid_id(Integer marketAction_Bid_id) {
		this.marketAction_Bid_id = marketAction_Bid_id;
	}

	public Integer getMarketAction_Offer_id() {
		return marketAction_Offer_id;
	}

	public void setMarketAction_Offer_id(Integer marketAction_Offer_id) {
		this.marketAction_Offer_id = marketAction_Offer_id;
	}

	public BigDecimal getExchangedValue() {
		return exchangedValue;
	}

	public void setExchangedValue(BigDecimal exchangedValue) {
		this.exchangedValue = exchangedValue;
	}

}
